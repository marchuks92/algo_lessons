#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#ifdef __GNUC__
#define PACK( __Declaration__ ) __Declaration__ __attribute__((__packed__))
#endif

#ifdef _MSC_VER
#define PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop))
#endif

PACK(struct bmpHeader_s {
    uint16_t signature;
    uint32_t fileSize;
    uint32_t reserved;
    uint32_t dataOffset;
});

struct infoHeader_s {
    uint32_t infoHeaderSize;
    uint32_t width;
    uint32_t height;
    uint16_t colorPlanesNum;
    uint16_t bitsPerPxl;
    uint32_t compression;
    uint32_t imageSize;
    uint32_t XpixelsPerM;
    uint32_t YpixelsPerM;
    uint32_t colorsImportant;
};

struct colorTable_s {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t reserved;
};

typedef PACK(struct {
    uint8_t blue;
    uint8_t green;
    uint8_t red;
}) pixel_s;

int printPixel(pixel_s* pxl)
{
    if (pxl->red > pxl->green && pxl->red > pxl->blue) {
        return printf("R");
    }
    else if (pxl->green > pxl->red && pxl->green > pxl->blue) {
        return printf("G");
    }
    else if (pxl->blue > pxl->red && pxl->blue > pxl->green) {
        return printf("B");
    }
    else {
        return printf(" ");
    }
}

int main(int argc, char* argv[])
{
    FILE* fptr = fopen(argv[1], "rb");
    struct bmpHeader_s bmpHeader;
    struct infoHeader_s infoHeader;
    pixel_s pxl;

    if (fptr == NULL)
    {
        printf("Error!");
        exit(1);
    }

    fread(&bmpHeader, sizeof(struct bmpHeader_s), 1, fptr);
    fread(&infoHeader, sizeof(struct infoHeader_s), 1, fptr);
    int rowSize = ((infoHeader.bitsPerPxl * infoHeader.width + 31) / 32) * 4;

    fseek(fptr, bmpHeader.dataOffset, SEEK_SET);
    for (int i = 0; i < infoHeader.width; i++) {
        for (int j = 0; j < infoHeader.height; j++) {
            fread(&pxl, sizeof(pxl), 1, fptr);
            printPixel(&pxl);
        }
        fseek(fptr, rowSize - infoHeader.height * infoHeader.bitsPerPxl / 8, SEEK_CUR);
        printf("\n");
    }

    fclose(fptr);
    return 0;
}