#include <stdio.h>

int main(int argc, char* argv[])
{
    for (unsigned char ch = 0; ch < 128; ch++) {
        printf("%02x\t%d\t%c\n", ch, ch, ch);
    }
    return 0;
}
