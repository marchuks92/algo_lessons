#include <stdio.h>

void cycle_while(int *arr, int len)
{
    int i = 0;
    printf("%s:\n", __func__);
    while (i < len) {
        printf("%d ", arr[i]);
        i++;
    }
    printf("\n");
}

void cycle_do_while(int *arr, int len)
{
    int i = 0;
    printf("%s:\n", __func__);
    do { 
        printf("%d ", arr[i]);
        i++;
    } while (i < len);
    printf("\n");
}

void cycle_for(int *arr, int len)
{
    printf("%s:\n", __func__);
    for (int i = 0; i < len; i++) { 
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void print_row(int *arr, int len)
{
    for (int i = 0; i < len; i++) { 
        printf("%2d ", arr[i]);
    }
}

void break_example()
{
    printf("%s:\n", __func__);
    int arr[10][10] = {0};
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            arr[i][j] = i * 10 + j;
            if (j == 5) {
                break;
            }
        }
    }

    for (int i = 0; i < 10; i++) {
        print_row(arr[i], 10);
        printf("\n");
    }
    printf("\n");
}

void continue_example()
{
    printf("%s:\n", __func__);
    int arr[10][10] = {0};
    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            if (j == 5) {
                continue;
            }
            arr[i][j] = i * 10 + j;
        }
    }

    for (int i = 0; i < 10; i++) {
        print_row(arr[i], 10);
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char* argv[])
{
    int arr[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    int *empty_arr;

    cycle_while(arr, 10);
    cycle_do_while(arr, 10);
    cycle_for(arr, 10);
/*
    cycle_while(arr, 0);
    cycle_do_while(arr, 0);
*/
/*
    cycle_while(NULL, 0);
    cycle_do_while(NULL, 0);
*/
    continue_example();
    break_example();
}