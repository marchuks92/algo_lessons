#!/bin/bash
C_FILE=$1
FILENAME=$(echo $1 | cut -f 1 -d '.')

# Preprocessor
gcc -E $C_FILE > "$FILENAME.preprocessed"

# Compiler
gcc -S $C_FILE -o "$FILENAME.s"

# Assembler
gcc -c "$FILENAME.s"

# Linker
gcc "$FILENAME.o" -o $FILENAME.bin

# Ececute
./$FILENAME.bin